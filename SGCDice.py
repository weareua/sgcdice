import re
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from components.engine import Engine
from components.start import start


def received_information(bot, update, user_data):
    text = update.message.text
    Engine.init(bot, update)
    engine = Engine()
    # If someone appeals to bot
    if re.match(r'(?i)(.*?)(дайс\w*|куб\w*)\s?', text):
        #Manners
        if re.match(r'(?i)(.*?)(п\и?р\о?\и?\ю?в\w?|д\а?о?\ров\w?|прю|алоха|оха\й?\о?\ё?|др\і?\и?с\ь?т\і?\и?)\s?', text): # noqa
            engine.greetings()
        elif re.match(r'(?i)(.*?)(\s?пасиб|\s?якую)\w?\s?', text):
            engine.manners()
        # Optionally pushes additional info about dice types
        engine.push_info()

    if re.match(r'(?i)(\d*)(d|к)(4|6|8|100|10|12|20)\s*\+?\s*(\-?\d*)', text):
        raw_groups = re.match(r'(?i)(\d*)(d|к)(4|6|8|100|10|12|20)\s*\+?\s*(\-?\d*)', text).groups()
        raw_numbers = raw_groups[:1] + raw_groups[2:]
        engine.roll(raw_numbers)


def request_handler():
    # bot token
    updater = Updater('412375790:AAEBAOVSBIvzZJju81XIILXKtz0RsaCsMxI')

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(MessageHandler(
        Filters.text, received_information, pass_user_data=True))

    updater.start_polling()
    updater.idle()
