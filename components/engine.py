import random
import time
import telegram
from components.lingua import greetings_list, thx_list


class Engine():
    """
    Creates engine object and proceed essential methods
    """
    inited = False

    @staticmethod
    def init(bot, update):
        """
        Init static members for entire class
        """
        Engine.bot = bot
        Engine.chat_id = update.message.chat_id
        Engine.sender_name = update.message.from_user.first_name
        Engine.inited = True

        Engine.bot.setWebhook()

    def greetings(self):
        """
        Generate and send greetings if someone appeals to bot
        """

        attr_list = [random.choice(greetings_list), self.sender_name]
        random.shuffle(attr_list)
        greetings_text = "{0}, {1}!".format(attr_list[0], attr_list[1])
        if not greetings_text[0].isupper():
            greetings_text = greetings_text[:1].capitalize() + greetings_text[1:]
        self.send(greetings_text)

    def manners(self):
        """
        Say thanks if someone does to the bot
        """
        attr_list = [random.choice(thx_list), self.sender_name]
        random.shuffle(attr_list)
        thx_text = "{0}, {1}.".format(attr_list[0], attr_list[1])
        if not thx_text[0].isupper():
            thx_text = thx_text[:1].capitalize() + thx_text[1:]
        self.send(thx_text)

    def roll(self, raw_groups):
        """
        Roll the dice. Returns result of multiplier, dice type and modifier
        """
        multiple, dice, modifier = self.get_numbers_from_groups(raw_groups)
        results_list = []
        dice_result = 0
        message = ""

        if multiple and multiple is not 0:
            for i in range(multiple):
                dice_result = random.randint(1, dice)
                results_list.append(dice_result)
        else:
            dice_result = random.randint(1, dice)
            results_list.append(dice_result)

        result = sum(results_list)
                
        if modifier:
            result = result + modifier

        if not multiple or multiple == 1:
            message = "<b>{0}</b>".format(dice_result)
        else:
            for number in results_list:
                message += "<b>{0}</b> + ".format(number)
            message = message[:-2]
        if modifier:
            message += " + {0} ".format(modifier)
        if (multiple and multiple != 1) or modifier:
            message += "= <b>{0}</b>".format(result)
        self.send(message)

    def info(self):
        """
        Dice types reminder
        """
        message = "Нагадую, що я віддаю перевагу класичним кубикам типу D4, D6, D8, D10, D12, D20 та D100."
        self.send(message)

    def typing_time(self):
        """
        Simulate typing action
        """
        seconds_list = range(1,5)
        self.bot.sendChatAction(
            chat_id=self.chat_id, action=telegram.ChatAction.TYPING)
        time.sleep(random.choice(seconds_list))

    def send(self, message):
        """
        Send proeeded message
        """
        self.typing_time()
        self.bot.sendMessage(
            chat_id=self.chat_id, text=message, parse_mode=telegram.ParseMode.HTML)

    def get_numbers_from_groups(self, raw_groups):
        """
        Check and prepare given numbers
        """
        if raw_groups[0] == "":
            multiple = None
        else:
            multiple = int(raw_groups[0])

        dice = int(raw_groups[1])

        if raw_groups[2] == "":
            modifier = None
        else:
            modifier = int(raw_groups[2])

        return multiple, dice, modifier

    def push_info(self):
        """
        Make sure that info displays not every time
        """
        quess_list = range(3)
        quess = random.choice(quess_list)
        if quess == 2:
            self.info()
